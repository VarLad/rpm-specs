%global debug_package %{nil}

Name:       fnott
Version:    1.7.1
Release:    1%{?dist}
Summary:    Keyboard driven and lightweight Wayland notification daemon for wlroots-based compositors.

License:    MIT
URL:        https://codeberg.org/dnkl/fnott
Source0:    %{url}/archive/%{version}.tar.gz

BuildRequires: fontconfig-devel
BuildRequires: pixman-devel
BuildRequires: nanosvg-devel
BuildRequires: tllist-devel
BuildRequires: wayland-protocols-devel
BuildRequires: libpng-devel
BuildRequires: cmake
BuildRequires: gcc
BuildRequires: wlroots-devel
BuildRequires: dbus-devel
BuildRequires: scdoc
BuildRequires: freetype-devel
BuildRequires: fcft-devel
BuildRequires: meson
BuildRequires: ninja-build
BuildRequires: wayland-devel
BuildRequires: python3

Requires: wlroots
Requires: fontconfig
Requires: pixman
Requires: freetype
Requires: libpng
Requires: libwayland-cursor
Requires: libwayland-client
Requires: fcft
Requires: dbus

%description
Fnott is a keyboard driven and lightweight notification daemon for wlroots-based Wayland compositors.

%prep
%autosetup -n fnott

%build
%meson -Db_lto=true
%meson_build

%install
%meson_install

strip --strip-all %{buildroot}%{_bindir}/*

%files
%{_bindir}/fnott
%{_bindir}/fnottctl

%{_mandir}/*

%dir %{_datadir}/doc/%{name}
%license %{_datadir}/doc/%{name}/LICENSE
%doc %{_datadir}/doc/%{name}/README.md
%doc CHANGELOG.md

%dir %{_sysconfdir}/xdg/%{name}/
%config(noreplace) %{_sysconfdir}/xdg/%{name}/fnott.ini
%{_datadir}/applications/fnott.desktop

%dir %{_datadir}/dbus-1
%dir %{_datadir}/dbus-1/services
%{_datadir}/dbus-1/services/%{name}.service

%{_datadir}/zsh/site-functions/_fnott
%{_datadir}/zsh/site-functions/_fnottctl

%changelog