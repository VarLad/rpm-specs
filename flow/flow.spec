%global debug_package %{nil}

Name:       flow
Version:    0.3.3
Release:    2%{?dist}
Summary:    Flow Control: a programmer's text editor

License:    MIT
URL:        https://github.com/neurocyte/flow
Source0:    %{url}/archive/refs/tags/v%{version}.tar.gz

BuildRequires: zig

%description
A modern terminal with 24bit color and kitty keyboard protocol support. Kitty, Foot and Ghostty are the only recommended terminals at this time. Most other terminals will work, but with reduced functionality.

%prep
%autosetup -p1

%install
zig build -Doptimize=ReleaseSmall --release=fast
mkdir -p %{buildroot}%{_bindir}
install -Dpm755 ./zig-out/bin/flow %{buildroot}%{_bindir}/flow
mkdir -p %{buildroot}%{_datadir}/licenses/flow
mv ./LICENSE %{buildroot}%{_datadir}/licenses/flow/LICENSE
mkdir -p %{buildroot}%{_docdir}/flow
mv README.md %{buildroot}%{_docdir}/flow/README.md

%files
%license LICENSE
%doc README.md
%{_bindir}/flow