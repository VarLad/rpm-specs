%global debug_package %{nil}

Name:       eww
Version:    0.6.0
Release:    4%{?dist}
Summary:    ElKowars wacky widgets.

License:    MIT
URL:        https://github.com/elkowar/eww
Source0:    %{url}/archive/refs/tags/v%{version}.tar.gz

%if 0%{?el8}
%else
BuildRequires: cargo >= 1.39
BuildRequires: rust >= 1.39
%endif
BuildRequires: gtk3-devel
BuildRequires: gtk-layer-shell-devel
BuildRequires: libdbusmenu-gtk3-devel
BuildRequires: glib2-devel
BuildRequires: libdbusmenu-devel

Requires: gtk3
Requires: libdbusmenu
Requires: gtk-layer-shell
Requires: libdbusmenu-gtk3
Requires: glib2

%description
Elkowars Wacky Widgets is a standalone widget system made in Rust that allows you to implement your own, custom widgets in any window manager.

%prep
%autosetup -p1

%build
%{cargo_build}

%install
install -Dm755 target/release/%{name} -t %{buildroot}%{_bindir}

%check
%if %{with test}
%{cargo_test}
%endif

%files
%license LICEN*
%doc README* docs examples CHANGELOG.md
%{_bindir}/%{name}

%changelog