%global debug_package %{nil}

Name:       roclang
Version:    mainf662f29
Release:    1%{?dist}
Summary:    A fast, friendly, functional language.

License:    UPL-1.0
URL:        https://github.com/roc-lang/roc/
Source0:    %{url}/archive/refs/heads/main.zip

BuildRequires: cargo
BuildRequires: rust
BuildRequires: gcc-c++
BuildRequires: llvm18-devel
BuildRequires: clang18-devel
BuildRequires: git
BuildRequires: zig
BuildRequires: libffi-devel
BuildRequires: zlib-devel

%description
A fast, friendly, functional language.

%prep
%autosetup -n roc-main

%install
cargo build --release
mkdir -p %{buildroot}%{_datadir}/roclang
mkdir -p %{buildroot}%{_libdir}
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_docdir}/roclang
mv ./target/release/roc %{buildroot}%{_bindir}/roc
mv ./target/release/roc-docs %{buildroot}%{_bindir}/roc-docs
mv ./target/release/roc_language_server %{buildroot}%{_bindir}/roc_language_server
mv ./target/release/roc_wasm_interp %{buildroot}%{_bindir}/roc_wasm_interp
mv ./target/release/libroc_repl_wasm.so %{buildroot}%{_libdir}/libroc_repl_wasm.so
mv ./target/release/lib/wasi-libc.a %{buildroot}%{_libdir}/wasi-libc.a

mv ./examples %{buildroot}%{_datadir}/roclang
mkdir -p %{buildroot}%{_datadir}/licenses/roclang
mv ./LICENSE %{buildroot}%{_datadir}/licenses/roclang/LICENSE
mv ./README.md %{buildroot}%{_docdir}/roclang/README.md
mv ./roc-for-elm-programmers.md %{buildroot}%{_docdir}/roclang/roc-for-elm-programmers.md

%files
%license LICENSE
%{_docdir}/roclang/roc-for-elm-programmers.md
%{_docdir}/roclang/README.md
%{_bindir}/roc
%{_bindir}/roc-docs
%{_bindir}/roc_language_server
%{_bindir}/roc_wasm_interp
%{_libdir}/wasi-libc.a
%{_libdir}/libroc_repl_wasm.so
%{_datadir}/roclang/examples/