%global debug_package %{nil}

Name:       zk
Version:    0.14.2
Release:    1%{?dist}
Summary:    A plain text note-taking assistant 

License:    MIT
URL:        https://github.com/zk-org/zk
Source0:    %{url}/archive/refs/tags/v%{version}.tar.gz

Requires: fzf
BuildRequires: golang
BuildRequires: openssl-devel
BuildRequires: git
BuildRequires: make
BuildRequires: gcc
%if 0%{?el8}
%else
BuildRequires: cargo >= 1.39
BuildRequires: rust >= 1.39
%endif

%description
cat for markdown: Show markdown documents in terminals

%prep
%autosetup -p1
make
mkdir -p %{buildroot}%{_bindir}
mv ./zk %{buildroot}%{_bindir}/zk

mkdir -p %{buildroot}%{_datadir}/licenses/zk
mv ./LICENSE %{buildroot}%{_datadir}/licenses/zk/LICENSE
mkdir -p %{buildroot}%{_docdir}/zk
mv README.md %{buildroot}%{_docdir}/zk/README.md

strip --strip-all %{buildroot}%{_bindir}/*

%files
%license LICENSE
%doc README.md
%{_bindir}/zk
