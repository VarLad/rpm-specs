%global debug_package %{nil}

Name:       yazi
Version:    25.2.11
Release:    1%{?dist}
Summary:    Blazing fast terminal file manager written in Rust, based on async I/O.

License:    MIT
URL:        https://github.com/sxyazi/yazi
Source0:    %{url}/archive/refs/tags/v%{version}.tar.gz

Requires: git
BuildRequires: git
BuildRequires: make
BuildRequires: gcc
%if 0%{?el8}
%else
BuildRequires: cargo >= 1.84.1
BuildRequires: rust >= 1.84.1
%endif

%description
Yazi (means "duck") is a terminal file manager written in Rust, based on non-blocking async I/O. It aims to provide an efficient, user-friendly, and customizable file management experience.

%prep
%autosetup -p1
%if 0%{?el8}
  curl https://sh.rustup.rs -sSf | sh -s -- --profile minimal -y
%endif

%install
export CARGO_PROFILE_RELEASE_BUILD_OVERRIDE_OPT_LEVEL=3
%if 0%{?el8}
  $HOME/.cargo/bin/cargo build --release --locked
%else
  cargo build --release --locked
%endif
mkdir -p %{buildroot}%{_bindir}
mv ./target/release/ya %{buildroot}%{_bindir}/ya
mv ./target/release/yazi %{buildroot}%{_bindir}/yazi

mkdir -p %{buildroot}%{_datadir}/licenses/yazi
mv ./LICENSE %{buildroot}%{_datadir}/licenses/yazi/LICENSE
mkdir -p %{buildroot}%{_docdir}/yazi
mv README.md %{buildroot}%{_docdir}/yazi/README.md

rm -f %{buildroot}%{_prefix}/.crates.toml \
    %{buildroot}%{_prefix}/.crates2.json
strip --strip-all %{buildroot}%{_bindir}/*

%files
%license LICENSE
%doc README.md
%{_bindir}/yazi
%{_bindir}/ya